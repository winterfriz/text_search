# -*- coding: utf-8 -*-

class Article:
    articles = []
    
    def __init__(self, url, content, polarity, subjectivity, \
                 organization_count, person_count): 
        self.url = url, 
        self.polarity = polarity,
        self.subjectivity = subjectivity,
        self.content = content, 
        self.organization_count = organization_count, 
        self.person_count = person_count
    
    @classmethod
    def add(cls, article):
        cls.articles.append(article)
    
    @classmethod
    def get_articles(cls):
        return cls.articles
    
    def __str__(self):
        return self.url
