# -*- coding: utf-8 -*-

from urllib.request import Request, urlopen
import gzip

import config


def get_wets():
    req = Request(config.WETS_URL)
    req.add_header('Accept-Encoding', 'gzip')
    response = urlopen(req)
    content = gzip.decompress(response.read())
    return content.splitlines()
    
# print(get_wets()[0:10])
# for line in decomp_req:
#     print(line.decode('utf-8'))