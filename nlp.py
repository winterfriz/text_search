# -*- coding: utf-8 -*-

from textblob import TextBlob
from spacy_langdetect import LanguageDetector
import spacy

def detect_lang(text):
    nlp = spacy.load('en_core_web_sm')
    nlp.add_pipe(LanguageDetector(), name='language_detector', last=True)
    doc = nlp(text)
    return doc._.language['language']

def get_doc_sentiment(text):
    sentiment = TextBlob(text).sentiment
    return sentiment
