# -*- coding: utf-8 -*-

import config
from wet import get_wets
from utils import RecordsCounter
import requests
from searcher import process_wet_group


if __name__ in '__main__':
    wets = get_wets()
    records_count = 0
    counter = RecordsCounter(config.RECORDS_LIMIT)
    for i, url in enumerate(iter(wets)):
        if counter.is_limit(): break
        
        print("*"*10)
        print(f'{i} - {url}')
        print("*"*10)
        full_url = f'{config.URL_WET_HEAD}{url.decode("utf8")}'
        resp = requests.get(full_url, stream=True)    
        process_wet_group(resp.raw, counter)
        print(counter.value())


