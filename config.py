# -*- coding: utf-8 -*-

URL_WET_HEAD = 'https://commoncrawl.s3.amazonaws.com/'
WETS_URL = f'{URL_WET_HEAD}crawl-data/CC-MAIN-2019-43/wet.paths.gz'
ORGANIZATION = b'Amazon'
PERSON = b'Bezos'
COUNT_ORGANIZATION_LIMIT = 2
COUNT_PERSON_LIMIT = 1
LANG = 'en'
RECORDS_LIMIT = 1000000