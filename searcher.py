# -*- coding: utf-8 -*-

from warcio.archiveiterator import ArchiveIterator

import config
from nlp import detect_lang, get_doc_sentiment
from article import Article

#TODO need to refactor
def process_wet_group(data, counter):
    iterator = ArchiveIterator(data, arc2warc=True)
    warcinfo = next(iterator)
    
    for record in iterator:
        if not counter.is_limit(): 
            counter.increment()
        else:
            break
        # if i == 10000: break

        content = record.raw_stream.read()
        count_organ = content.count(config.ORGANIZATION)

        if count_organ >= config.COUNT_ORGANIZATION_LIMIT:
            count_person = content.count(config.PERSON)

            if count_person >= config.COUNT_PERSON_LIMIT:
                decoded_content = content.decode()
                lang = detect_lang(decoded_content)

                if lang == config.LANG:
                    uri = record.rec_headers.get_header('WARC-Target-URI')
                    sentiment = get_doc_sentiment(decoded_content)

                    article = Article(
                        url=uri,
        #                     date=date,
                        content=content,
                        polarity=sentiment.polarity,
                        subjectivity=sentiment.subjectivity,
                        organization_count=count_organ,
                        person_count=count_person)
                    Article.add(article)

                    print(uri)
