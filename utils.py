# -*- coding: utf-8 -*-

import config

class RecordsCounter:
    def __init__(self, limit):
        self.count = 0
        self.limit = limit
    
    def increment(self):
        self.count += 1
    
    def value(self):
        return self.count
    
    def is_limit(self):
        if self.count >= config.RECORDS_LIMIT:
            return True
        return False